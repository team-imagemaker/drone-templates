#!/bin/bash
echo ""
echo ""

result=$(drone template ls)
for entry in $(find "$PWD" -type f -not -path "$(pwd)/.git/*")
do
    filename="$(basename -- $entry)"
    extension="${filename##*.}"

    if [ $extension = "starlark" ] || [ $extension = "jsonnet" ]; then
        python3 compile.py "$entry" "$entry"

        if [ -z "${result##*$filename*}" ] ;then
            echo "-> Actualizando '$filename' ..."
            drone template update --namespace "$DRONE_NAMESPACE" --name "$filename" --data "@$entry"
            if [ "$?" != "0" ]; then
                echo "Ha ocurrido un error."
                exit 123
            fi
        else
            echo "-> Agregando '$filename' ..."
            drone template add --namespace "$DRONE_NAMESPACE" --name "$filename" --data "@$entry"
            if [ "$?" != "0" ]; then
                echo "Ha ocurrido un error."
                exit 123
            fi
        fi
    fi
done