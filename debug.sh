#!/bin/bash
echo ""
echo ""

for entry in $(find "$PWD" -type f -not -path "$(pwd)/.git/*")
do
    filename="$(basename -- $entry)"
    extension="${filename##*.}"

    if [ $extension = "starlark" ] || [ $extension = "jsonnet" ]; then
        echo "-> Procesando '$filename' ..."
        python3 compile.py "$entry" "$entry.output"
    fi
done