import sys
import re

def readAll(file):
    f = open(file, 'r')
    retorno = f.read()
    f.close()
    return retorno

def writeAll(file, text):
    f = open(file, 'w')
    f.write(text)
    f.close()

filename=sys.argv[1]
outputfilename=sys.argv[2]
text=readAll(filename)
importedFiles=[]

result = re.search("(<<[^>]*>>)", text) 
while result != None:
    importStatement = result.group(0)

    if importStatement not in importedFiles:
        text = text.replace(importStatement, readAll(importStatement.replace("<<","").replace(">>", "")))
        importedFiles.append(importStatement)
    else:
        text = text.replace(importStatement, "")

    result = re.search(".*(<<[^>]*>>).*", text)

writeAll(outputfilename, text)