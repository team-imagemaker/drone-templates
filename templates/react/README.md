# Reference

```yaml
kind: template
load: react.starlark
data:
  gitOpsRepository: https://bitbucket.org/team-imagemaker/<cliente>-<proyecto>-gor.git
  [event: push]
  [Build:]
    command: ""
    commands:
    - <command1>
    - <command2>
    node_version: ""
    resources:
      limits:
        memory: ""
      requests:
        memory: ""
```