# Reference

```yaml
kind: template
load: <plantilla>.starlark
data:
  gitOpsRepository: https://bitbucket.org/team-imagemaker/<cliente>-<proyecto>-gor.git
  [event: push]
  [Build:]
    command: ""
    gradle_version: ""
    resources:
      limits:
        memory: ""
      requests:
        memory: ""
```