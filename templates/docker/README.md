# Reference

```yaml
kind: template
load: docker.starlark
data:
  [Dockerize]:
    image: ibm-websphere-8.5.5.14
    tag: latest
```