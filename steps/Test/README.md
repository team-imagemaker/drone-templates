kind: template
load: <name>.starlark
data:
  [Test]:
    [enabled: (true)]

    [Selenium]:
      [chrome: 10]
      [edge: 5]
      [firefox: 3]

      steps:
      - name: caso1
        image: "python"
        [enabled: (true)]
        commands:
        - "pip install selenium"
        - "python ./case1.py"
      
      [Azure:]
        [uploadReport: (true)]
        [storageURL: https://{storageAccount}.blob.core.windows.net/[folder1/subfolder1]] (DevOps Storage)
        [secretTokenName: (az_storage_sas_token)]

 ----------------------------------------------------------------

# Detalle

La url para conectarse es: http://selenium:4444/wd/hu

En cada step tienes acceso a las siguientes variables de entorno:

    DRONE_COMMIT: "bcdd4bf0245c82c060407b3b24b9b87301d15ac1",
    DRONE_BRANCH: "develop",
    DRONE_COMMIT_AUTHOR: "octocat",
    DRONE_COMMIT_AUTHOR_EMAIL: "octocat@mail.com"
    DRONE_REPO_NAME: "hello-world"

[] = Opcional
() = Valor por defecto
El nombre sin signos = Obligatorio si se define su padre


# Uso en Python

Ejemplo

```python
  opt = webdriver.ChromeOptions()
  opt.add_argument("--start-fullscreen")
  opt.add_argument("--headless")
  opt.add_argument("--no-sandbox")
  opt.add_argument("--disable-dev-shm-usage")
  opt.add_argument("--window-size={}x{}".format(self.screen_width, self.screen_height))

  driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub', options=opt)
  driver.maximize_window()

```
