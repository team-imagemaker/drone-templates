# Reference

```yaml
kind: template
load: <plantilla>.starlark
data:
  [event: push]
  Load:
    Tests:
    - name: loadtest1
      host: http://dev.sadasd.svc/
      [enabled: (true)]
      [timeout: 3m]
      [users: (100)]
      [spawn: (10)]
      [workers: (1)]
      [gui: (false)]
      [file: (/drone/src/locustfile.py)]
      [report: (/drone/src/report.html)]
      K8s:
        [metricsURL: http://k8s.domain.com/metrics/<namespace>/pods/] (DevOps Cluster)
        [namespace: (default)]
        [secretTokenName: (k8s_metrics_token)]
        containers:
        - "[{namespace}.]{pod1}[:{container1},{container2}]"
        - "[{namespace}.]{pod2}[:{container1},{container2}]"
      [Azure:]
        [uploadReport: (true)]
        [storageURL: https://{storageAccount}.blob.core.windows.net/[folder1/subfolder1]] (DevOps Storage)
        [secretTokenName: (az_storage_sas_token)]
```